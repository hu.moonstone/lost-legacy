(asdf:defsystem :lost-legacy-test
  :serial t
  :pathname "test/lisp"
  :components ((:file "package")
               (:file "lost-legacy-test"))
  :depends-on (:asdf :fiveam :lost-legacy))
