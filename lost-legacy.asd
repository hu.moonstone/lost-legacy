(asdf:defsystem :lost-legacy
  :serial t
  :pathname "src/lisp"
  :components ((:file "package")
               (:file "lost-legacy"))
  :depends-on (:asdf :sdl2 :sdl2-image))
