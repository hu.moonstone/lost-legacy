(in-package :lost-legacy)

(defparameter *screen-width* 960)
(defparameter *screen-height* 540)
(defparameter *window* nil)
(defparameter *screen* nil)
(defparameter *renderer* nil)
(defparameter *image* nil)
(defparameter *image-tex* nil)

(defparameter *input-state* (make-hash-table))
(defparameter *pos-x* 120)
(defparameter *pos-y* 0)

(defun update-input-state (key value)
  (setf (gethash key *input-state*) value))

(defun input-state (key)
  (gethash key *input-state*))

(defun init-input-state-table ()
  (update-input-state 'UP nil)
  (update-input-state 'RIHGT nil)
  (update-input-state 'DOWN nil)
  (update-input-state 'LEFT nil)
  (update-input-state 'TRIGGER-R1 nil)
  (update-input-state 'TRIGGER-R2 nil)
  (update-input-state 'TRIGGER-L1 nil)
  (update-input-state 'TRIGGER-L2 nil)
  (update-input-state 'START nil)
  (update-input-state 'SELECT nil)
  (update-input-state 'A nil)
  (update-input-state 'B nil)
  (update-input-state 'X nil)
  (update-input-state 'Y nil))

(defun output-input-state ()
  (if (input-state 'UP)
      (print "UP"))
  (if (input-state 'RIGHT)
      (print "RIHGT"))
  (if (input-state 'DOWN)
      (print "DOWN"))
  (if (input-state 'LEFT)
      (print "LEFT")))

(defun make-window ()
  (sdl2:create-window :title "SDL2 Window"
                      :x 0
                      :y 0
                      :w *screen-width*
                      :h *screen-height*
                      :flags '(:shown)))


(defun get-keysym (event)
  (plus-c:c-ref event sdl2-ffi:sdl-event :key :keysym))

(defun key-state (keysym key)
  (sdl2:scancode= (sdl2:scancode-value keysym) key))

(defun input-process (event event-type)
  (output-input-state)

  (case event-type
    (:WINDOWEVENT
     )
    (:IDLE
     )
    (:MOUSEMOTION
     )
    (:MOUSEBUTTONUP
     )
    (:MOUSEBUTTONDOWN
     ;; (print (sdl2::get-event-code event-type))
     ;; (print (sdl2::get-user-data event-id))
     )
    (:KEYUP
     (let ((keysym (get-keysym event)))
       (when (key-state keysym :scancode-escape)
         (sdl2:push-event :quit))
       (when (key-state keysym :scancode-w)
         (update-input-state 'UP nil))
       (when (key-state keysym :scancode-d)
         (update-input-state 'RIGHT nil))
       (when (key-state keysym :scancode-s)
         (update-input-state 'DOWN nil))
       (when (key-state keysym :scancode-a)
         (update-input-state 'LEFT nil))
       ))

    (:KEYDOWN
     (let ((keysym (get-keysym event)))
       (when (key-state keysym :scancode-w)
         (update-input-state 'UP t))
       (when (key-state keysym :scancode-d)
         (update-input-state 'RIGHT t))
       (when (key-state keysym :scancode-s)
         (update-input-state 'DOWN t))
       (when (key-state keysym :scancode-a)
         (update-input-state 'LEFT t))
       ))
    )
  )

(defun update-process ()
  (if (input-state 'UP)
      (setf *pos-y* (- *pos-y* 1)))
  (if (input-state 'RIGHT)
      (setf *pos-x* (+ *pos-x* 1)))
  (if (input-state 'DOWN)
      (setf *pos-y* (+ *pos-y* 1)))
  (if (input-state 'LEFT)
      (setf *pos-x* (- *pos-x* 1)))
  )


(defun draw ()
  (sdl2:set-render-draw-color *renderer* 0 0 255 255)
  (sdl2:render-clear *renderer*)

  (sdl2:set-render-draw-color *renderer* 255 0 255 255)
  (sdl2:query-texture *image-tex*)
  (sdl2:render-draw-rect *renderer* (sdl2:make-rect 300 300 100 100))
  (sdl2:render-fill-rect *renderer* (sdl2:make-rect 445 400 35 35))
  (sdl2:render-copy *renderer* *image-tex*
                    :source-rect (sdl2:make-rect 0 0 32 32)
                    :dest-rect (sdl2:make-rect *pos-x* *pos-y* 32 32))

  (sdl2:render-present *renderer*)
  ;;(sdl2:update-window *window*)
  )

(defun main-loop (event)
  (let ((event-quit))
    (loop
       :until event-quit
       :do
         (sdl2:next-event event :poll nil)
         (let* ((event-type (sdl2:get-event-type event))
                (event-id
                 (and (sdl2::user-event-type-p event-type)
                      (event :user :code))))
           ;; ウインドウクローズイベント処理
           (if (eq :QUIT event-type)
               (setf event-quit t))

           (unless event-quit
             ;; 入力受付
             (input-process event event-type)
             ;; パラメータ更新
             (update-process)
             ;; 描画処理
             (draw)
             ;; ウェイト
             (sdl2:delay 10))))))

(defparameter *renderer* nil)

(defun game-main ()
  (init-input-state-table)
  (if (not (= (sdl2:init :video)))
      (print "Error")
      (print "success"))
  (print (sdl2:was-init :video))

  (setf *window* (make-window))
  (setf *screen* (sdl2:get-window-surface *window*))

  (setf *renderer* (sdl2:create-renderer *window* nil '(:software)))

  (setf *image* (sdl2-image:load-image "resources/images/ball.png"))

  (sdl2:set-color-key *image* :true (sdl2:map-rgb (sdl2:surface-format *image*) 0 #xFF 0))

  (setf *image-tex*  (sdl2:create-texture-from-surface *renderer* *image*))

  (sdl2:query-texture *image-tex*)

  (let ((event nil))
    (unwind-protect (setf sdl2::*event-loop* t)
      (sdl2:in-main-thread
       (:background nil)
       (sdl2:with-sdl-event (event)
         (main-loop event)))
      (setf sdl2::*event-loop* nil)))

  (sdl2:free-surface *image*)
  (sdl2:destroy-texture *image-tex*)
  (sdl2:destroy-renderer *renderer*)
  (sdl2:destroy-window *window*)
  (sdl2:quit))

(defun main ()
  (game-main))
